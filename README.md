# NodeMongoPractices

Practicas de mongo y node <br>

Requerimientos: <br>
1.  Instalar mongoDB
2.  Instalar node y npm
3.  Para gestionar bases de datos mongo se puede usar el IDE Robo 3T

Despues de clonar el proyecto <br>
<code>git clone https://gitlab.com/cdaza/nodemongopractices.git</code>
        
Se debe ejecutar la instalacion de las dependencias<br>
<code>npm install</code>
    
Para correr los ejemplos se debe ejecutar<br>
<code>node archivoEjecutar.js</code>
    
Por ejemplo para ejecutar primerEjemplo.js<br>
<code>node primerEjemplo.js</code>
   