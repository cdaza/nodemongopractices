// importa el mongoClient del modulo de MongoDB
const { MongoClient } = require('mongodb');

//configuracion mongoDB
const url = 'mongodb://localhost:27017/primerEjemplo',
    nombreBaseDatos = "primerEjemplo",
    nombreColeccionEjemplo = "capitulos";

// Objeto con capitulos a insertar en mongoDB
let capitulos = [{
    'Titulo': 'Primer Paso',
    'Autor': "Pedro Perez"
},{
    'Titulo': 'Segundo Paso',
    'Autor': "Pepe Perez"
},{
    'Titulo': 'Tercer Paso',
    'Autor': "Roberto Perez"
}];

MongoClient.connect(url, { useNewUrlParser: true }, function(error, cliente){
    console.log("conectado correctamente al servidor")

    //obtener base de datos
    let db = cliente.db(nombreBaseDatos)   

    //obtener coleccion 
    let coleccion = db.collection(nombreColeccionEjemplo);

    //Elimina todos los documentos en la coleccion
    coleccion.deleteMany({}, function(error, resultado){
        if (error) {
            console.log("han ocurrido errores")
            throw error;
        }else{
            console.log("Se han eliminado todos los documentos")
        }
    });

    //Insertar datos
    coleccion.insertMany(capitulos, function(error, resultado){
        if (error) {
            console.log("han ocurrido errores")
            throw error;
        }else{
            console.log("Insercion exitosa :" + resultado.ops.length + " Capitulos insertados")
        }
    });

    //Eliminar datos
    coleccion = db.collection(nombreColeccionEjemplo);
    let query = {Autor: 'Pepe Perez'}
    coleccion.deleteOne(query, function(error, resultado){
        if (error) {
            console.log("han ocurrido errores")
            throw error;
        }else{
            console.log("Documento eliminado"+ resultado)
        }
    });

    //Actualizar datos
    coleccion = db.collection(nombreColeccionEjemplo);
    query = {Autor: 'Pedro Perez'}
    let nuevoValor ={ $set: {Titulo: "Primer Gran Paso"}}
    coleccion.updateOne(query, nuevoValor, function(error, resultado){
        if (error) {
            console.log("han ocurrido errores")
            throw error;
        }else{
            console.log("Documento Actualizado"+ resultado)
        }   
    });

    //Buscar datos
    coleccion = db.collection(nombreColeccionEjemplo);
    coleccion.find({}).toArray(function(error, resultado){
        if (error) {
            console.log("han ocurrido errores")
            throw error;
        }else{
            resultado.forEach(elemento => {
                console.log(`documento encontrado: ${elemento.Titulo} - ${elemento.Autor}`)
            });
            
        }   
    });

    cliente.close()
});




